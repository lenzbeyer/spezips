Wie lang kann man ein Pferd mit einer Flasche Spezi betreiben?

Benötigte Energie um ein Pferd eine Sekunde zu betreiben:

```math
{1PS = 735,49875 \frac J s}
```

Energieinhalt Flasche Spezi: 190 kJ

```math
 Betriebsdauer = {  190 kJs \over 735,49875 J} = 4 min 18 Sekunden
```
